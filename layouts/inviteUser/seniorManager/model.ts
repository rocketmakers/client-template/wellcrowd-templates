/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string
  returnUrl: string
  appUrl: string
}

export const sampleData: IModel[] = [
  {
    username: "test@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
    appUrl: "https://organisation.wellcrowd.co.uk/login",
  },
  {
    username: "test2@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
    appUrl: "https://organisation.wellcrowd.co.uk/login",
  },
]
