/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string;
  returnUrl: string;
  portalUrl: string;
  appUrl: string;
  isAppUser: boolean;
  isPortalUser: boolean;
}

export const sampleData: IModel[] = [
  {
    username: "test@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
    portalUrl: "https://admin.wellcrowd.co.uk/login",
    appUrl: "https://app.wellcrowd.co.uk/login",
    isAppUser: true,
    isPortalUser: false,
  },
  {
    username: "test2@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
    portalUrl: "https://admin.wellcrowd.co.uk/login",
    appUrl: "https://app.wellcrowd.co.uk/login",
    isAppUser: false,
    isPortalUser: true,
  },
  {
    username: "test3@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
    portalUrl: "https://admin.wellcrowd.co.uk/login",
    appUrl: "https://app.wellcrowd.co.uk/login",
    isAppUser: true,
    isPortalUser: true,
  },
  {
    username: "test4@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
    portalUrl: "https://staging-admin.wellcrowd.co.uk/login",
    appUrl: "https://staging-app.wellcrowd.co.uk/login",
    isAppUser: true,
    isPortalUser: true,
  }
];
