/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  code: string;
}

export const sampleData: IModel[] = [
  {
    code: 'aaa',
  },
  {
    code: 'khj233bfgkdb',
  },
];
